# Information / Информация

SPEC-файл для создания RPM-пакета **zstd**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/zstd`.
2. Установить пакет: `dnf install zstd`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)